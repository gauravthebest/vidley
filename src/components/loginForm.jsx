import React from 'react';
import { Redirect } from 'react-router-dom';
import Joi from 'joi-browser';
import Form from './common/form';
import auth from '../services/authService';

class LoginForm extends Form {

  state = {
    data: {
      username: '',
      password: ''
    },  
    errors: {}
  }

  schema = {
    username: Joi.string().required().label('Username'),
    password: Joi.string().required().label('Password')
  }

  doSubmit = async () => {
    try {
      await auth.login(this.state.data);
      const { state } = this.props.location;
      console.log(state);
      window.location = state ? state.from.pathname : '/';
    } catch(err) {
      if(err.response && err.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = err.response.data;
        this.setState({ errors });
      }
    }
  }

  render() {
    if(auth.getCurrentUser()) return <Redirect to="/" />
    return (
      <div className="container col-md-7 login-form">
        <h2>Login</h2>
        <form onSubmit={this.handleSubmit}>
          { this.renderInput('username', 'Username') }
          { this.renderInput('password', 'Password', 'password') }
          { this.renderButton('Login') }
        </form>
      </div>
    );
  }
}

export default LoginForm;