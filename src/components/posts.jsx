import React, { Component } from 'react';
import Axios from 'axios';

Axios.interceptors.response.use(null, error => {
  console.log('Interceptor called');
  return Promise.reject(error);
});

class Posts extends Component {

  async componentDidMount() {
    let { data: posts } = await Axios.get("https://jsonplaceholder.typicode.com/posts");
    console.log(posts);
  }

  render() {
    return (
      <h1>POSTS</h1>
    );
  }

}
 
export default Posts;