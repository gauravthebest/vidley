import React from 'react';
import Joi from 'joi-browser';
import Form from './common/form';
import { getMovie, saveMovie } from '../services/moviesService';
import { getGenres } from '../services/genreService';

class AddMovie extends Form {

  state = {
    data: {
      title: '',
      genreId: '',
      numberInStock: '',
      dailyRentalRate: ''
    },
    genres: [],
    errors: {}
  }

  schema = {
    _id: Joi.string(),
    title: Joi.string().required().min(5).label('Title'),
    genreId: Joi.string().required().label('Genre'),
    numberInStock: Joi.number().integer().min(1).max(100).label('Number in Stock'),
    dailyRentalRate: Joi.number().min(1).max(10).label('Rate')
  }

  async populateGenres() {
    const { data: genres } = await getGenres();
    this.setState({ genres });
  }

  async populateMovies() {
    try {
      const movieId = this.props.match.params.id;
      if(movieId === 'new') return;
      const { data: movie } = await getMovie(movieId);
      this.setState({ data: this.mapToViewModel(movie) });
    } catch(err) {
      if(err.response && err.response.status === 404 ) 
        this.props.history.replace('/404');
    }
  }

  async componentDidMount() {
    await this.populateGenres();
    await this.populateMovies();
  }

  mapToViewModel = (movie) => {
    return {
      _id: movie._id,
      title: movie.title,
      genreId: movie.genre._id,
      numberInStock: movie.numberInStock,
      dailyRentalRate: movie.dailyRentalRate
    }
  }

  doSubmit = async () => {
    await saveMovie(this.state.data);
    this.props.history.push('/movies');
  }

  render() {
    return (
      <div className="container col-md-7 login-form">
        <h2>Movie Form</h2>
        <form onSubmit={this.handleSubmit}>
          { this.renderInput('title', 'Title') }
          { this.renderSelect('genreId', 'Genre', this.state.genres) }
          { this.renderInput('numberInStock', 'Number in Stock') }
          { this.renderInput('dailyRentalRate', 'Rate') }
          { this.renderButton('Save') }
        </form>
      </div>
    );
  }
}

export default AddMovie;