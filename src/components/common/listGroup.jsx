import React from 'react';

const ListGroup = (props) => {
  const { items, textProperty, valueProperty, seletedItem, onItemSelect } = props;
  return (
    <ul className="list-group">
      {items.map(item => <li 
        className={seletedItem === item ? "list-group-item active" : "list-group-item"} 
        key={item[textProperty] || Math.random() }
        onClick={() => onItemSelect(item)}>
        { item[valueProperty] }</li>)}
    </ul>
  );
}

ListGroup.defaultProps = {
  textProperty: '_id',
  valueProperty: 'name'
}

export default ListGroup;