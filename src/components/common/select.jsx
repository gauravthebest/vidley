import React from 'react';

const Select = ({ name, label, error, options, ...rest }) => {
  return (
    <div className="form-group">
      <label htmlFor="">{label}</label>
      <select name={name} id={name} {...rest} className="form-control">
        <option value=""></option>
        { options.map(option => (
          <option value={ option._id } key={option._id}>{ option.name }</option>
        ))}
      </select>
      {error && <div className="alert alert-danger">{ error }</div>}
    </div>
  );
}

export default Select;