import React from 'react';
import Joi from 'joi-browser';
import auth from '../services/authService';
import Form from './common/form';
import { register } from '../services/userService';

class RegisterForm extends Form {

  state = {
    data: {
      username: '',
      password: '',
      name: ''
    },
    errors: {}
  }

  schema = {
    username: Joi.string().required().email().label('Username'),
    password: Joi.string().required().min(5).label('Password'),
    name: Joi.string().required().min(5).label('Name')
  }

  doSubmit = async () => {
    try {
      let { data } = await register(this.state.data);
      auth.loginWithJwt(data.token);
      window.location = '/';
    } catch(err) {
      if(err.response && err.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.username = err.response.data;
        this.setState({ errors });
      }
    }
  }

  render() {
    return (
      <div className="container col-md-7 login-form">
        <h2>Register</h2>
        <form onSubmit={this.handleSubmit}>
          { this.renderInput('username', 'Username') }
          { this.renderInput('password', 'Password', 'password') }
          { this.renderInput('name', 'Name') }
          { this.renderButton('Register') }
        </form>
      </div>
    );
  }
}

export default RegisterForm;