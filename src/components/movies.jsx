import React, { Component } from "react";
import _ from 'lodash';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import { getGenres }  from "../services/genreService";
import { getMovies, deleteMovie } from "../services/moviesService";
import Pagination from './common/pagination';
import Paginate from '../utils/paginate';
import ListGroup from './common/listGroup';
import MoviesTable from '../components/moviesTable';
import SearchBox from '../components/common/searchBox';

class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    pageSize: 4,
    currentPage: 1,
    selectedGenre: null,
    searchQuery: '',
    sortColumn: { path: 'title', order: 'asc'}
  };

  async componentDidMount() {
    let { data: apiGenres } = await getGenres();
    let { data: movies } = await getMovies();
    const genres = [{ _id: '', name: 'All Generes' }, ...apiGenres];
    this.setState({ movies, genres });
  }

  getPagedData = () => {
    const {
      pageSize,
      currentPage,
      selectedGenre,
      sortColumn,
      searchQuery,
      movies:allMovies} = this.state;


    let filtered = allMovies;
    if(searchQuery) {
      filtered = allMovies.filter(m =>
        m.title.toLowerCase().startsWith(searchQuery.toLowerCase())
      );
    } else if(selectedGenre && selectedGenre._id) {
      filtered = allMovies.filter(m => m.genre._id === selectedGenre._id);
    }

    const sortedMovies = _.orderBy(filtered, [sortColumn.path], [sortColumn.order]);

    const movies = Paginate(sortedMovies, currentPage, pageSize);

    return { totalCount: filtered.length, data: movies };
  }

  handleDelete = async (movie) => {
    const originalMovies = this.state.movies;
    const movies = originalMovies.filter(m => m._id !== movie._id);
    this.setState({ movies });
    try {
      await deleteMovie(movie._id);
    } catch(err) {
      toast.error('This movie has allready been deleted!');
    }
  };

  handleLike = movie => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index] = { ...movies[index] };
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };

  handlePageChange = (pageNumber) => {
    this.setState({currentPage: pageNumber});
  }

  handleGenreSelect = (genre) => {
    this.setState({ selectedGenre: genre, searchQuery: "", currentPage: 1 });
  }

  handleSort = (sortColumn) => {
    this.setState({ sortColumn });
  }

  handleChange = (query) => {
    this.setState({ searchQuery: query, selectedGenre: null, currentPage: 1 });
  }

  render() {
    const { length: count } = this.state.movies;
    const {
      pageSize,
      currentPage,
      genres,
      selectedGenre,
      sortColumn,
      searchQuery } = this.state;

    const {totalCount, data: movies} = this.getPagedData();

    const { user } = this.props;

    return (
      <div className="row movies-page">
        <div className="col-3">
          <ListGroup
            items={genres}
            onItemSelect={this.handleGenreSelect}
            seletedItem={selectedGenre}
          />
        </div>
        <div className="col">
          {user && 
            <Link to="/movies/new" className="btn btn-primary">New Movie</Link>
          }
          <p className="pt-4">Showing {totalCount} movies in the database.</p>
          <SearchBox value={searchQuery} onChange={this.handleChange} />
          <MoviesTable
            movies={movies}
            sortColumn={sortColumn}
            onLike={this.handleLike}
            onDelete={this.handleDelete}
            onSort={this.handleSort} />
          <Pagination
            itemCount={totalCount}
            pageSize={pageSize}
            currentPage={currentPage}
            onPageChange={this.handlePageChange}/>
        </div>
      </div>
    );
  }
}

export default Movies;
