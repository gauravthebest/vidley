import React, { Component } from 'react';
import Input from './common/input';

class LoginFormOld extends Component {

  state = {
    account: {
      username: '',
      password: ''
    },
    errors: {}
  }

  validate = () => {
    const errors = {};
    const { account } = this.state;
    if (account.username.trim() == '') {
      errors.username = 'username is required!';
    }
    if (account.password.trim() == '') {
      errors.password = 'password is required!';
    }
    return Object.keys(errors).length < 1 ? null : errors;
  }

  validateProperty = ({ name, value }) => {
    if (name == 'username') {
      if (value.trim() == '') {
        return 'username is required!';
      }
    }
    if (name == 'password') {
      if (value.trim() == '') {
        return 'password is required!';
      }
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    const errors = this.validate();
    this.setState({ errors: errors || {} });
    if (errors) return;
    console.log('Submitted');
  }

  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMesssage = this.validateProperty(input);
    if (errorMesssage) errors[input.name] = errorMesssage;
    else delete errors[input.name];

    const account = { ...this.state.account };
    account[input.name] = input.value;

    this.setState({ account, errors });
  }

  render() {
    const { account, errors } = this.state;
    return (
      <div className="container col-md-7">
        <h2>Login</h2>
        <form onSubmit={this.handleSubmit}>
          <Input 
            name="username"
            label="Username"
            value={account.username}
            error={errors.username}
            onChange={this.handleChange}
          />
          <Input 
            name="password"
            label="Password"
            value={account.password}
            error={errors.password}
            onChange={this.handleChange}
          />
          <button className="btn btn-primary">Login</button>
        </form>
      </div>
    );
  }
}

export default LoginFormOld;