import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Navbar from './components/navbar';
import Logout from './components/logout';
import Movies from './components/movies';
import MovieForm from './components/movieForm';
import PageNotFound from './components/404';
import LoginForm from './components/loginForm';
import RegisterForm from './components/registerForm';
import Posts from './components/posts';
import ProtectedRoute from './components/common/protectedRoute';
import auth from './services/authService';
import './App.css';

class App extends Component {
  state = {}

  componentDidMount() {
    let user = auth.getCurrentUser();
    this.setState({ user });
  }

  render() {
    return (
      <React.Fragment>
        <Navbar user={this.state.user} />
        <main className="container">
          <Switch>
            <Route path='/posts' component={Posts} />            
            <Route path='/login' component={LoginForm} />
            <Route path='/logout' component={Logout} />
            <Route path='/register' component={RegisterForm} />
            <ProtectedRoute path='/movies/:id' component={MovieForm} />
            <Route 
              path='/movies'
              render={ props => <Movies {...props} user={this.state.user} />} 
            />
            <Route path='/404' component={PageNotFound} />
            <Redirect from='/' exact to='/movies' />
            <Redirect to='/404' />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default App;
