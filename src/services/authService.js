import jwtDecode from 'jwt-decode';
import http from './httpService';

const apiEndpoint = "auth";
const tokenKey = 'token';

http.setJwt(getJwt());

async function login(user) {
  let { data } = await http.post(apiEndpoint, {
    email: user.username,
    password: user.password
  });
  localStorage.setItem(tokenKey, data);
}

function loginWithJwt(jwt) {
  localStorage.setItem(tokenKey, jwt);
}

function logout() {
  localStorage.removeItem(tokenKey);
}

function getCurrentUser() {
  try {
    let jwt = localStorage.getItem(tokenKey);
    return jwtDecode(jwt);
  } catch(err) {
    return null;
  }
}

function getJwt() {
  return localStorage.getItem('token');
}

export default {
  login,
  loginWithJwt,
  getCurrentUser,
  logout
}